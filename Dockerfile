FROM node:8.11.3-alpine

ENV TZ=Asia/Bangkok
RUN apk update
RUN apk upgrade
RUN apk add ca-certificates && update-ca-certificates
RUN apk add --update tzdata
RUN rm -rf /var/cache/apk/*

RUN npm install --no-optional  pm2 -g 
RUN pm2 -v 

RUN npm install --no-optional -g npm 
RUN node -v  

WORKDIR /app/management-system
COPY . .

CMD node server.js
# CMD pm2 start  pm2.json