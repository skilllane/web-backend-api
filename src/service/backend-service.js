const userManager = require('../user/service/user-manager');
const webBusinessManagerr = require('../web-business/service/web-business-manager');
const courseManager = require('../course/service/course-manager');

class BackendService {
    buildUserService() {
        return userManager;
    }
    buildWebBusinessService() {
        return webBusinessManagerr;
    }
    buildCourseService() {
        return courseManager;
    }
}

module.exports = BackendService;