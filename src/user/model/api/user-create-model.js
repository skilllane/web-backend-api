class UserCreateModel {
    constructor(_data){
        this.username = _data.username ;
        this.password = _data.password ;
        this.firstname = _data.firstname ;
        this.lastname = _data.lastname ;
        this.nickname = _data.nickname ;
        this.birthday = _data.birthday ;
        this.type = _data.type ;
        this.gender = _data.gender ;
        this.courseid = _data.courseid ;
    }
}

module.exports = UserCreateModel;