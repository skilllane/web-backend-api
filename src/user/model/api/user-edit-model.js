class UserEditModel{
    constructor(_data){
        this.firstname  = _data.firstname ;
        this.lastname   = _data.lastname ;
        this.nickname   = _data.nickname ;
        this.gender     = _data.gender ;
    }
}

module.exports = UserEditModel ;