const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username: { type: String },
    password: { type: String },
    firstname: { type: String },
    lastname: { type: String },
    nickname: { type: String },
    birthday: { type: String },
    type: { type: String },
    gender: { type: String },
    courseid: { type: String }
});

module.exports = mongoose.model('user', UserSchema);
