const sha256 = require('sha256');

const UserDBManager = require('../service/db/userdb-manager');

//Models
const UserCreateModel = require('../model/api/user-create-model');
const UserEditModel   = require('../model/api/user-edit-model');

class UserManager{
    async createUser(req, res){
        try {
            await UserDBManager.connectDB();
            var password = sha256(req.body.password);
            req.body.password = password ;
            const userCreateModel = new UserCreateModel(req.body);
            // console.log(userCreateModel)
            const createUser = await UserDBManager.createUser(userCreateModel).catch(err => {
                res.status(err).send();
            });
            // console.log(JSON.stringify(createUser))
            res.status(createUser.status).send({ _id: createUser.data._id });
            
        } catch (error) {
            res.status(500).send();
        }
    }

    async updateUser(req, res){
        try {
            await UserDBManager.connectDB();
            const userEditModel = new UserEditModel(req.body);
            const iduser = req.params.iduser;
            const userEdit = await UserDBManager.editUser(iduser, userEditModel)
                .catch(err => {
                    res.status(err).send();
                });
            res.status(userEdit).send();
        } catch (error) {
            res.status(500).send();
        }
    }

    async searchUser(req, res){
        try {
            await UserDBManager.connectDB();

            var url = require('url');
            var url_parts = url.parse(req.url, true);
            var query = url_parts.query;

            let _id = query._id;

            let searchBy = {};

            if(_id){ searchBy._id = query._id; }
            const searchUser = await UserDBManager.searchUser( searchBy )
                .catch(err => {
                    res.status(err).send();
                });
            res.status(200).send(searchUser);
        } catch (error) {
            res.status(500).send();
        }
    }
}

const userManager = new UserManager();
module.exports = userManager;