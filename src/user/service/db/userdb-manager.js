const UserSchema = require('../../model/db/user-schema');
const mongoose = require('mongoose');

class UserDBManager{

    connectDB(){
        return new Promise((resolve, reject) => {
        try {
            mongoose
                .connect(
                    "mongodb://localhost:27017/db"  ,
                    {
                        useNewUrlParser: true,
                        connectTimeoutMS: 2000
                    }
                )
                .then((result) => {
                    console.log('connectDb Success');
                    resolve();
                })
                .catch(err => {
                    // console.log('connectDb Error');
                    reject(503);
                })
        } catch (error) {
            
        }
        });
    }

    createUser(_data){
        return new Promise((resolve, reject) => {
            try {
                UserSchema.find({ username: _data.username })
                    .then(result => {
                        if (result.length == 0) {
                            const userSchema =  new UserSchema(
                                _data
                            );
                            userSchema.save().then(result => {
                                // console.log('saved');
                                resolve({ status: 201, data: result });
                            })
                            .catch(err => {
                                reject(503);
                            });
                        } else {
                            reject(422);
                        }
                    })
                    .catch(err => {
                        reject(503);
                    });
                    
            } catch (error) {
                reject(500);
            }
        });
    }

    editUser(iduser, _data){
        return new Promise((resolve, reject) => {
            try {
                UserSchema.updateOne({ _id: iduser },  _data )
                .then(result => {
                    resolve(200);
                })
                .catch(err => {
                    reject(503);
                });
            } catch (error) {
                reject(500);
            }
        });
    }

    searchUser(searchBy){
        return new Promise((resolve, reject) => {
            try {
                UserSchema.find( searchBy )
                    .then(result => {
                        resolve(result);
                    })
                    .catch(err => {
                        reject(503);
                    });
            } catch (error) {
                reject(500);
            }
        });
    }

}

const userDbManager = new UserDBManager();
module.exports = userDbManager;