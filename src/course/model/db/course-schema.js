const mongoose = require('mongoose');

const CourseSchema = mongoose.Schema({
    coursename: { type: String },
    usercreate: { type: String },
    description: { type: String },
    category: { type: String },
    subject: { type: String },
    starttime: { type: String },
    endtime: { type: String },
    studentid: []
});

module.exports = mongoose.model('course', CourseSchema);