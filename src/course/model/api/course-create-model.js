class CourseCreateModel {
    constructor(_data) {
        this.coursename = _data.coursename;
        this.usercreate = _data.usercreate;
        this.description = _data.description;
        this.category = _data.category;
        this.subject = _data.subject;
        this.starttime = _data.starttime;
        this.endtime = _data.endtime;
        this.studentid = _data.studentid;
    }
}

module.exports = CourseCreateModel;