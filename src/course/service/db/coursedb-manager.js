const mongoose = require('mongoose');

const CourceSchema = require('../../model/db/course-schema');

class CourseDBManager {

    connectDB() {
        return new Promise((resolve, reject) => {
            try {
                mongoose
                    .connect(
                        "mongodb://localhost:27017/db",
                        {
                            useNewUrlParser: true,
                            connectTimeoutMS: 2000
                        }
                    )
                    .then((result) => {
                        console.log('connectDb Success');
                        resolve();
                    })
                    .catch(err => {
                        // console.log('connectDb Error');
                        reject(503);
                    })
            } catch (error) {

            }
        });
    }

    createCourse(_data) {
        return new Promise((resolve, reject) => {
            try {
                CourceSchema.find({ coursename: _data.coursename })
                .then(result => {
                    if (result.length == 0) {
                        const courseSchema = new CourceSchema(_data);
                        courseSchema.save()
                            .then(result => {
                                resolve(201);
                            })
                            .catch(err => {
                                reject(503);
                            })
                    } else {
                        reject(422);
                    }
                })
                .catch(err => {
                    reject(503);
                })
                
            } catch (error) {
                reject(500);
            }
        });
    }

    searchCourse(searchBy){
        return new Promise((resolve, reject) => {
            try {
                CourceSchema.find( searchBy )
                    .then(result => {
                        resolve(result);
                    })
                    .catch(err => {
                        reject(503);
                    });
            } catch (error) {
                reject(500);
            }
        });
    }

}

const courseDBManager = new CourseDBManager();
module.exports = courseDBManager;