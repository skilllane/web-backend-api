const CourseDBManager = require('../service/db/coursedb-manager');

//model
const CourseCreateModel = require('../model/api/course-create-model');

class CourseManager {

    async createCourse(req, res) {
        try {
            await CourseDBManager.connectDB();
            const courseCreateModel = new CourseCreateModel(req.body);
            const createCourse = await CourseDBManager.createCourse(courseCreateModel)
                .catch(err => {
                    res.status(err).send();
                });
            res.status(createCourse).send();
        } catch (error) {
            res.status(500).send();
        }
    }

    async searchCourse(req, res){
        try {
            await CourseDBManager.connectDB();

            var url = require('url');
            var url_parts = url.parse(req.url, true);
            var query = url_parts.query;

            let _id = query._id;
            let coursename = query.coursename;
            let starttime = query.starttime;
            let endtime = query.endtime;

            let searchBy = {};
            
            if(_id){ searchBy._id = query._id; }
            if(coursename){ searchBy.coursename = query.coursename; }
            if(starttime){ searchBy.starttime = query.starttime; }
            if(endtime){ searchBy.endtime = query.endtime; }
            
            const searchCourse = await CourseDBManager.searchCourse( searchBy )
                .catch(err => {
                    res.status(err).send();
                });
            res.status(200).send(searchCourse);
        } catch (error) {
            res.status(500).send();
        }
    }

}

const courseManager = new CourseManager();
module.exports = courseManager;