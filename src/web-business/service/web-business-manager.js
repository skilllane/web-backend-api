const sha256 = require('sha256');

const UserDBManager = require('../../user/service/db/userdb-manager');

class WebBusinessManager{
    async login(req, res){
        try {
            const username = req.body.username ;
            const password = sha256(req.body.password || '') ;
            const searchBy = { username: username, password: password } ;
            // console.log('username :'+username)
            // console.log('password :'+password)
            await UserDBManager.connectDB();
            const searchUser = await UserDBManager.searchUser(searchBy)
                .catch(err => {
                    res.status(err).send();
                });
            if (searchUser.length == 0) {
                res.status(404).send();
            } else {
                res.status(200).send(searchUser[0]);
            }
        } catch (error) {
            res.status(500).send();
        }
    }
}

const webBusinessManager = new WebBusinessManager();
module.exports = webBusinessManager;