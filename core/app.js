const express = require('express');
const bodyparser = require('body-parser');
var cors = require('cors')
const path = require('path');

const app = express();
const jsonParser = bodyparser.json();

const BackendService = require('../src/service/backend-service');

app.use(bodyparser.urlencoded({ extended: true }));
app.use(cors())
app.use("/*", (req, res, next)=> {
   res.setHeader("Access-Control-Allow-Origin", "*");
   res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
   res.setHeader("Access-Control-Allow-Methods","GET, POST, PATCH, PUT, DELETE, OPTIONS");
   next();
})
//user
app.post('/api/user', jsonParser, (req, res) => {
    const backendService = new BackendService();
    const userService = backendService.buildUserService();
    userService.createUser(req, res);
});

app.patch('/api/user/:iduser', jsonParser, (req, res) => {
    const backendService = new BackendService();
    const userService = backendService.buildUserService();
    userService.updateUser(req, res);
});

app.get('/api/users/q', (req, res) => {
    const backendService = new BackendService();
    const userService = backendService.buildUserService();
    userService.searchUser(req, res);
});

//web
app.post('/api/web/login', jsonParser, (req, res) => {
    const backendService = new BackendService();
    const webBusinessService = backendService.buildWebBusinessService();
    webBusinessService.login(req, res);
});

//course
app.post('/api/course', jsonParser, (req, res) => {
    const backendService = new BackendService();
    const courseService = backendService.buildCourseService();
    courseService.createCourse(req, res);
});

app.get('/api/courses/q', (req, res) => {
    const backendService = new BackendService();
    const courseService = backendService.buildCourseService();
    courseService.searchCourse(req, res);
});

app.use(express.static(__dirname + '/dist/web-fontend'));
app.use(function(req, res){
    res.sendFile(path.resolve(__dirname + '/dist/web-fontend/index.html'));
});


module.exports = app;